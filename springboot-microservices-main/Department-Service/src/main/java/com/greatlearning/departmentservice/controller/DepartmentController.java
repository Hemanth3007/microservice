package com.greatlearning.departmentservice.controller;

import com.greatlearning.departmentservice.entity.Department;
import com.greatlearning.departmentservice.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/departments")
@Slf4j
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @PostMapping("/")
    public Department saveDepartment(@RequestBody Department department){
        log.info("Inside SaveDepartment method of DepartmentController");
        return departmentService.saveDepartment(department);
    }

    @GetMapping("/{id}")
    public Department findDepartmentByDepartmentId(@PathVariable("id") Long departmentId){
        log.info("Inside findDepartmentById method of Department Controller");
        return departmentService.findDepartmentByDepartmentId(departmentId);
    }
}
